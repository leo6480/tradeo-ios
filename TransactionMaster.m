//
//  TransactionMaster.m
//  tradeoProject
//
//  Created by student5305 on 12/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import "TransactionMaster.h"
#import "CompanyMaster.h"
#import "TransactionHistMaster.h"


@implementation TransactionMaster

@dynamic companyName;
@dynamic companyTag;
@dynamic moneyGain;
@dynamic number;
@dynamic percentGain;
@dynamic taxePercent;
@dynamic total;
@dynamic unitPrice;
@dynamic positive;
@dynamic company;
@dynamic transactionHistList;

@end
