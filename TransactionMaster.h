//
//  TransactionMaster.h
//  tradeoProject
//
//  Created by student5305 on 12/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CompanyMaster, TransactionHistMaster;

@interface TransactionMaster : NSManagedObject

@property (nonatomic, retain) NSString * companyName;
@property (nonatomic, retain) NSString * companyTag;
@property (nonatomic, retain) NSNumber * moneyGain;
@property (nonatomic, retain) NSNumber * number;
@property (nonatomic, retain) NSNumber * percentGain;
@property (nonatomic, retain) NSNumber * taxePercent;
@property (nonatomic, retain) NSNumber * total;
@property (nonatomic, retain) NSNumber * unitPrice;
@property (nonatomic, retain) NSNumber * positive;
@property (nonatomic, retain) CompanyMaster *company;
@property (nonatomic, retain) NSSet *transactionHistList;
@end

@interface TransactionMaster (CoreDataGeneratedAccessors)

- (void)addTransactionHistListObject:(TransactionHistMaster *)value;
- (void)removeTransactionHistListObject:(TransactionHistMaster *)value;
- (void)addTransactionHistList:(NSSet *)values;
- (void)removeTransactionHistList:(NSSet *)values;

@end
