//
//  OrderTableViewCell.h
//  tradeoProject
//
//  Created by Costabile Léonard on 10/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderTableViewCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UILabel *companyTag;
@property (strong, nonatomic) IBOutlet UILabel *actionSense;
@property (strong, nonatomic) IBOutlet UILabel *titlesNumberValue;
@property (strong, nonatomic) IBOutlet UILabel *stockPriceValue;

@property (strong, nonatomic) IBOutlet UILabel *minRangeValue;
@property (strong, nonatomic) IBOutlet UILabel *maxRangeValue;


@end
