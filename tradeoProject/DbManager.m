//
//  DbManager.m
//  iOS7-ex5
//
//  Created by student5305 on 14/05/14.
//  Copyright (c) 2014 masociete. All rights reserved.
//

#import "DbManager.h"

@interface DbManager()

- (void) initDb:(NSString*)dbName;

@end

@implementation DbManager

- (id) initWithDbName:(NSString *)dbName{

    self = [super init];
    
    if(self != nil){
    
        [self initDb:dbName];
    }
    
    return self;
}

- (void) initDb:(NSString*)dbName {
    
    // Accès au dossier /Documents
    NSArray *documentsPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    self.documentsDirectoryPath = [documentsPaths objectAtIndex:0];
    
    self.managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    self.persistenceCoordinator = [[NSPersistentStoreCoordinator alloc]
                                   initWithManagedObjectModel:self.managedObjectModel];
    NSError *error = nil;
    NSURL *urlStorage = [NSURL fileURLWithPath:[self.documentsDirectoryPath
                                                stringByAppendingPathComponent:dbName]];
    [self.persistenceCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                              configuration:nil
                                                        URL:urlStorage
                                                    options:nil
                                                      error:&error];
    self.managedObjectContext = [[NSManagedObjectContext alloc] init];
    [self.managedObjectContext setPersistentStoreCoordinator:self.persistenceCoordinator];
    
    NSLog(@"error creation : %@",error);
}

- (NSEntityDescription *) getEntityDescriptionFor:(NSString *)entity {

    NSEntityDescription *entityDescription= [NSEntityDescription entityForName:entity inManagedObjectContext:self.managedObjectContext];
    
    
    
    return entityDescription;
}


- (id) initClass:(NSString *)className {

    return [[NSClassFromString(className) alloc] initWithEntity:[self getEntityDescriptionFor:className] insertIntoManagedObjectContext:self.managedObjectContext];
    
    
}

- (void) saveObject {

    NSError *error = nil;
    [self.managedObjectContext save:&error];
  
    NSLog(@"error save : %@",error);
    
    if(error != nil){
       
    }
}

- (void) deleteObject:(NSManagedObject *)object {

    NSError *error = nil;
    [self.managedObjectContext deleteObject:object];
    [self.managedObjectContext save:&error];
    
    if(error != nil){
        
    }
}

- (NSArray *) fetchAll:(NSString *)entity {

    NSFetchRequest *fetchEntities = [NSFetchRequest fetchRequestWithEntityName:entity];
    
    NSError *errorFetch = nil;
    
    NSArray *entities = [self.managedObjectContext executeFetchRequest:fetchEntities error:&errorFetch];
    
    NSLog(@"fetchAll error : %@",errorFetch);
    
    return entities;
}

- (NSArray *) fetch:(NSString *)entity where:(id)whereObj {
    
    NSArray *entities = nil;
    
    if([whereObj isKindOfClass:[NSString class]]){
    
        NSFetchRequest *fetchEntities = [NSFetchRequest fetchRequestWithEntityName:entity];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:whereObj];
        [fetchEntities setPredicate:predicate];
        entities = [self.managedObjectContext executeFetchRequest:fetchEntities error:nil];

    }
    else if([whereObj isKindOfClass:[NSPredicate class]]){
    
        NSFetchRequest *fetchEntities = [NSFetchRequest fetchRequestWithEntityName:entity];
        [fetchEntities setPredicate:whereObj];
        entities = [self.managedObjectContext executeFetchRequest:fetchEntities error:nil];
    }

    return entities;
    
}




- (NSPredicate*) addWhere:(NSArray*)whereArray {

    if([whereArray count] != 0){
        
        self.predicate = [NSCompoundPredicate andPredicateWithSubpredicates:whereArray];
    }
    
    return self.predicate;
}

- (NSPredicate*) where:(NSString*)format value:(id)value {
    
    return [NSPredicate predicateWithFormat:format, value];
}



/*
 EXAMPLE MULTIPE PREDICATE
 
 NSPredicate *p1 = [NSPredicate predicateWithFormat:@"place CONTAINS[cd] %@", placeTextField.text];
 NSPredicate *p2 = [NSPredicate predicateWithFormat:@"category CONTAINS[cd] %@", selectedCategory];
 NSPredicate *p3 = [NSPredicate predicateWithFormat:@"(dates >= %@) AND (dates <= %@)", selectedFromDate,selectedToDate];
 NSPredicate *p4 = [NSPredicate predicateWithFormat:@"(amount >= %f) AND (amount <= %f)", [amountFromTextField.text floatValue],[amountToTextField.text floatValue]];
 
 NSPredicate *placesPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[p1, p2, p3, p4]];
 */

@end
