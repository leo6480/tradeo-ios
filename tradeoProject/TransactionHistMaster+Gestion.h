//
//  TransactionHistMaster+Gestion.h
//  tradeoProject
//
//  Created by student5305 on 12/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import "TransactionHistMaster.h"

@interface TransactionHistMaster (Gestion)

-(id) initWithNumber:(int)number unitPrice:(float)unitPrice action:(NSString*)action;

@end
