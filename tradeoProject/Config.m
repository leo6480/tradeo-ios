//
//  Config.m
//  tradeoProject
//
//  Created by student5305 on 12/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import "Config.h"
#import "DbManager.h"

@implementation Config

static NSString *dbName = @"tradeo";
static DbManager *dbManager;
static NSString *devise = @"$";
static NSString *minCashWallet = @"20000";

+(NSString*)getDbName {

    return dbName;
}

+(DbManager*)getDbManager {
    
    if(dbManager == nil){
    
        dbManager = [[DbManager alloc] initWithDbName:dbName];
    }

    return dbManager;
}

+(NSString*)getDevise {

    return devise;
}

+(NSNumber*)getMinCashWallet {

    return [NSNumber numberWithFloat:[minCashWallet floatValue]];
    
}

@end
