//
//  WalletMaster+Gestion.m
//  tradeoProject
//
//  Created by student5305 on 12/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import "WalletMaster+Gestion.h"
#import "TransactionMaster+Gestion.h"
#import "DbManager.h"
#import "Config.h"

@implementation WalletMaster (Gestion)

+ (id) getWallet {
    
    NSArray *data = [[Config getDbManager] fetchAll:@"WalletMaster"];
    
    WalletMaster *wallet = nil;
    
    if([data count] != 0){
        
        wallet = [data objectAtIndex:0];
    }
    
    return wallet;
}

- (float) getTotal {
    
    float total = 0;

    self.transactionList = [[NSSet alloc] initWithArray:[[Config getDbManager] fetchAll:@"TransactionMaster"]];

    for(TransactionMaster *tr in self.transactionList){
        
        total += [[tr getTotal] floatValue];
    }
    
    return total;
    
}

@end
