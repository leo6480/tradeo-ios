//
//  CompanyTransactionVC.h
//  tradeoProject
//
//  Created by Costabile Léonard on 9/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DbManager.h"
#import "TransactionMaster.h"
#import "BuySellTiltesDelegate.h"

@interface CompanyTransactionVC : UIViewController<BuySellTiltesDelegate>


@property (strong, nonatomic) TransactionMaster *mCompanyTransaction;
@property (strong, nonatomic) CompanyMaster *mCompany;

#pragma mark - IB OUTLET

@property (strong, nonatomic) IBOutlet UILabel *companyTag;
@property (strong, nonatomic) IBOutlet UILabel *stockPriceValue;
@property (strong, nonatomic) IBOutlet UILabel *titlesNumberValue;
@property (strong, nonatomic) IBOutlet UILabel *totalValue;
@property (strong, nonatomic) IBOutlet UILabel *moneyGainValue;
@property (strong, nonatomic) IBOutlet UILabel *percentGainValue;

@property (strong, nonatomic) IBOutlet UILabel *stockPriceLabel;

@property (strong, nonatomic) IBOutlet UILabel *titlesNumberLabel;

@property (strong, nonatomic) IBOutlet UILabel *totalLabel;

@property (strong, nonatomic) IBOutlet UIButton *shareButton;






#pragma mark - IB ACTION

- (IBAction)didTapShare:(id)sender;
- (IBAction)didTapBuySell:(id)sender;



@end
