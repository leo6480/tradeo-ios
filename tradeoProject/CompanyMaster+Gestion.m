//
//  CompanyMaster+Gestion.m
//  tradeoProject
//
//  Created by student5305 on 12/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import "CompanyMaster+Gestion.h"

@implementation CompanyMaster (Gestion)

- (NSMutableArray*) getListFromString:(NSString*)stockEvolution {
    
    NSMutableArray* array = [[NSMutableArray alloc] init];
    
    NSArray *tableau = [stockEvolution componentsSeparatedByString:@","];
    
    for(NSString *string in tableau){
        
        if(![string isEqualToString:@""]){
            
            NSNumber *value = [NSNumber numberWithFloat:[string floatValue]];
            
            [array addObject:value];
        }
    }
    
    return array;
    
    
}
- (NSString*) getStringFromList:(NSMutableArray*)stockEvolution {
    
    NSString *string = @"";
    
    for(NSNumber *value in stockEvolution){
        
        string = [[string stringByAppendingString:[value stringValue]] stringByAppendingString:@","];
    }
    
    return string;
    
}

- (NSArray*) getStringArrayFromList:(NSMutableArray*)stockEvolution {

    NSMutableArray *mutableArray = [[NSMutableArray alloc] init];
    NSString *string = @"";
    
    for(NSNumber *value in stockEvolution){
        
        string = [value stringValue];
        [mutableArray addObject:string];
    }
    
    return [NSArray arrayWithArray:mutableArray];

}

- (NSMutableArray*) getStockEvolutionList {
    
    return [self getListFromString:[self stockEvolution]];
}

- (void) setStockEvolutionWithList:(NSMutableArray*)stockEvolution {
    
    self.stockEvolution = [self getStringFromList:stockEvolution];
}


-(float) getStockPrice {
    
    NSMutableArray *prices = [self getListFromString:[self stockEvolution]];
    
    float price = [[prices lastObject] floatValue];
    
    return price;
}

@end
