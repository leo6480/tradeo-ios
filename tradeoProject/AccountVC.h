//
//  AccountVC.h
//  tradeoProject
//
//  Created by Costabile Léonard on 9/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderDelegate.h"
#import "ValidSellTitlesDelegate.h"
#import "GainDelegate.h"

@interface AccountVC : UIViewController <UITableViewDelegate, UITableViewDataSource, OrderDelegate, ValidSellTitlesDelegate, GainDelegate>

@property (strong, nonatomic) IBOutlet UILabel *walletValue;
@property (strong, nonatomic) IBOutlet UILabel *cashValue;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *transactionsList;
@property (nonatomic, retain) NSTimer *timer;
@property (weak, nonatomic) IBOutlet UIButton *buttonClock;


- (IBAction)didTapOrderWaiting:(id)sender;


@end
