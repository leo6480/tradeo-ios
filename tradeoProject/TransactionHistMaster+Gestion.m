//
//  TransactionHistMaster+Gestion.m
//  tradeoProject
//
//  Created by student5305 on 12/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import "TransactionHistMaster+Gestion.h"

@implementation TransactionHistMaster (Gestion)

-(id) initWithNumber:(int)number unitPrice:(float)unitPrice action:(NSString*)action {
    
    self = [super init];
    
    if(self != nil){
        
        [self setNumber:[NSNumber numberWithInt:number]];
        [self setUnitPrice:[NSNumber numberWithFloat:unitPrice]];
        [self setAction:action];
    }
    
    return self;
    
}

@end
