//
//  StockEvolution.m
//  tradeo
//
//  Created by Costabile Léonard on 8/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import "StockEvolution.h"

@implementation StockEvolution

- (id) initWithCompany:(CompanyMaster*)company {
    
        self = [super init];
        
        if(self != nil){
            
            [self setCompany:company];
        }
        
        return self;
}

@end
