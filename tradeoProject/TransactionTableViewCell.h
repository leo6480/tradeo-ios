//
//  TransactionTableViewCell.h
//  tradeoProject
//
//  Created by Costabile Léonard on 9/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TransactionTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *companyTag;
@property (strong, nonatomic) IBOutlet UILabel *titlesNumber;
@property (strong, nonatomic) IBOutlet UILabel *unitPrice;
@property (strong, nonatomic) IBOutlet UILabel *percentGain;

@end
