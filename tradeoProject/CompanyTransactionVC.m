//
//  CompanyTransactionVC.m
//  tradeoProject
//
//  Created by Costabile Léonard on 9/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import "CompanyTransactionVC.h"
#import "TitlesBuySellVC.h"
#import "DbManager.h"
#import "TransactionMaster.h"
#import "TransactionMaster+Gestion.h"
#import "CompanyMaster+Gestion.h"
#import "Config.h"
#import "NSString+withDecimals.h"
#import "Wallet.h"
#import "OrderMaster.h"

@interface CompanyTransactionVC ()

-(void)fillView;

@end

@implementation CompanyTransactionVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"Company";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    if(self.mCompany != nil){
    
        NSPredicate *where = [[Config getDbManager] where:@"companyTag =%@" value:[self.mCompany tag]];
        NSArray *data = [[Config getDbManager] fetch:@"TransactionMaster" where:where];
        
        if([data count] != 0){
            
            self.mCompanyTransaction = [data objectAtIndex:0];
            [self.mCompanyTransaction setCompany:self.mCompany];
        }
    }
    
    [self fillView];
    
    
}

-(void)fillView {
    
    if(self.mCompany != nil){
    
        self.companyTag.text = self.mCompany.tag;
        NSString *stockPriceString = [NSString stringWithTwooDecimalsForFloatValue:[self.mCompany getStockPrice]];
        self.stockPriceValue.text = [NSString stringWithFormat:@"%@ %@", stockPriceString, [Config getDevise]];
        
        if(self.mCompanyTransaction != nil){
        
            [self.mCompanyTransaction calculateGain];
            self.titlesNumberValue.text = [[self.mCompanyTransaction number] stringValue];
            self.totalValue.text = [[self.mCompanyTransaction getTotal] stringValue];
            
            // Operateur à afficher pour le gain en pourcentage
            
            NSString *operateurPercent = nil;
            
            if([self.mCompanyTransaction.positive boolValue] == YES){
                
                self.percentGainValue.textColor = [UIColor greenColor];
                operateurPercent = @"+";
            }
            else{
                
               self.percentGainValue.textColor = [UIColor redColor];
               operateurPercent = @"-";
            }
   
            self.percentGainValue.text = [NSString stringWithFormat:@"%@ %@ %%", operateurPercent, [[self.mCompanyTransaction percentGain] stringValue]];
            
            // Operateur à afficher pour le gain en argent
            
            NSString *operateurMoney = nil;
            
            if([self.mCompanyTransaction.positive boolValue] == YES){
                
                self.moneyGainValue.textColor = [UIColor greenColor];
                operateurMoney = @"+";
            }
            else{
                
                self.moneyGainValue.textColor = [UIColor redColor];
                operateurMoney = @"";
            }

            self.moneyGainValue.text = [NSString stringWithFormat:@"%@ %@ %@", operateurMoney, [[self.mCompanyTransaction moneyGain] stringValue], [Config getDevise]];
            
            //Button Share (pour plus tard)

        }
        else {
            
            [self.titlesNumberLabel setHidden:TRUE];
            [self.totalLabel setHidden:TRUE];
            [self.shareButton setHidden:TRUE];
            [self.titlesNumberValue setHidden:TRUE];
            [self.totalValue setHidden:TRUE];
            [self.moneyGainValue setHidden:TRUE];
            [self.percentGainValue setHidden:TRUE];
        }
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)didTapShare:(id)sender {
}

- (IBAction)didTapBuySell:(id)sender {
    
    TitlesBuySellVC *titlesBuySellVC = [[TitlesBuySellVC alloc] initWithNibName:@"TitlesBuySellVC" bundle:nil];
    titlesBuySellVC.delegate = self;
    
    titlesBuySellVC.existTransaction = (self.mCompanyTransaction != nil);
    titlesBuySellVC.mCompanyCourse = [NSNumber numberWithFloat:[self.mCompany getStockPrice]];
    
    if(titlesBuySellVC.mCompanyTransaction != nil){
    
        titlesBuySellVC.mCompanyTransactionQte = [titlesBuySellVC.mCompanyTransaction number];
    }
    else{
        titlesBuySellVC.mCompanyTransactionQte = [NSNumber numberWithInt:0];
    }

    [self presentViewController:titlesBuySellVC animated:YES completion:nil];
}

#pragma mark - BuySellTiltesDelegate

-(void)buySellTiltesGesture:(BOOL)validation action:(NSString*)action number:(NSNumber*)number rangeMin:(NSNumber*)rangeMin rangeMax:(NSNumber*)rangeMax {
    
    if(validation == YES){
        
        OrderMaster *order = nil;
    
        if(action != nil && number != nil){
        
            if(rangeMin == nil){ rangeMin = [NSNumber numberWithFloat:-1]; }
            
            if(rangeMax == nil){ rangeMax = [NSNumber numberWithFloat:-1]; }
            
            if([action isEqualToString:[Wallet labelForBuyAction]] && self.mCompany != nil){
            
                order = (OrderMaster*)[[Config getDbManager] initClass:@"OrderMaster"];
                [order setCompanyTag:[self.mCompany tag]];
                [order setPrice:[NSNumber numberWithFloat:[self.mCompany getStockPrice]]];
                [order setNumber:number];
                [order setMinRange:rangeMin];
                [order setMinRange:rangeMax];
                [order setAction:[Wallet labelForBuyAction]];
            
            }
            else if([action isEqualToString:[Wallet labelForSellAction]] && self.mCompany != nil){
            
                order = (OrderMaster*)[[Config getDbManager] initClass:@"OrderMaster"];
                [order setCompanyTag:[self.mCompany tag]];
                [order setPrice:[NSNumber numberWithFloat:[self.mCompany getStockPrice]]];
                [order setNumber:number];
                [order setMinRange:rangeMin];
                [order setMinRange:rangeMax];
                [order setAction:[Wallet labelForSellAction]];
            
            }
            
            if(order != nil){
            
                [[Config getDbManager] saveObject];
            }
            
        }
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
