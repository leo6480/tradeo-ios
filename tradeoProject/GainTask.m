//
//  GainTask.m
//  tradeo
//
//  Created by Costabile Léonard on 9/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import "GainTask.h"

@interface GainTask()

@end

@implementation GainTask

-(id)init {
    
    self = [super init];
    
    if(self != nil){
        
    }
    
    return self;
}

- (void) startTimer:(int)seconds {

    // Lancement une première fois (le timer n'appelle pas directement remindTask)
    [self.gainDelegate updateGain];
    
    self.timer = [NSTimer scheduledTimerWithTimeInterval:seconds
                                                  target: self
                                                selector: @selector(remindTask:)
                                                userInfo: nil
                                                 repeats: YES];
}

- (void) remindTask:(id)sender {

    [self.gainDelegate updateGain];
}

@end
