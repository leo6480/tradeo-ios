//
//  NSString+withDecimals.h
//  tradeoProject
//
//  Created by student5305 on 12/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (withDecimals)

+ (NSString*)stringWithTwooDecimalsForFloatValue:(float)floatValue;

+ (NSString*)stringWithTwooDecimalsForDoubleValue:(double)doubleValue;

@end
