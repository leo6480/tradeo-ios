//
//  BuySellTiltesDelegate.h
//  tradeoProject
//
//  Created by Costabile Léonard on 12/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol BuySellTiltesDelegate <NSObject>

-(void)buySellTiltesGesture:(BOOL)validation action:(NSString*)action number:(NSNumber*)number rangeMin:(NSNumber*)rangeMin rangeMax:(NSNumber*)rangeMax;

@end
