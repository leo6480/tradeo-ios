//
//  Config.h
//  tradeoProject
//
//  Created by student5305 on 12/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DbManager.h"

@interface Config : NSObject

+(NSString*)getDbName;
+(DbManager*)getDbManager;
+(NSString*)getDevise;
+(NSNumber*)getMinCashWallet;


@end
