//
//  TitlesBuySellVC.m
//  tradeoProject
//
//  Created by Costabile Léonard on 10/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import "TitlesBuySellVC.h"
#import "Config.h"
#import "WalletMaster.h"
#import "WalletMaster+Gestion.h"
#import "Wallet.h"

@interface TitlesBuySellVC ()

-(void)displayView;
-(void) displayError:(NSString *)message;
-(BOOL) dataCheckIntegrity:(NSString*)action number:(NSString*)number rangeMin:(NSString*)rangeMin rangeMax:(NSString*)rangeMax;


@end

@implementation TitlesBuySellVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.mWallet = [WalletMaster getWallet];
   
    [self displayView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
}

-(void)dismissKeyboard {
    
    [self.titlesNumberValue resignFirstResponder];
    [self.minRangeValue resignFirstResponder];
    [self.maxRangeValue resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)didTapCancel:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)didTapValidate:(id)sender {
    
    self.errorsCount = 0;
    NSString *action = [[NSString alloc] init];
    
    // Segment Buy selected
    if (self.segmentedControl.selectedSegmentIndex == 0){
        
        if([self dataCheckIntegrity:[Wallet labelForBuyAction] number:self.titlesNumberValue.text rangeMin:self.minRangeValue.text rangeMax:self.maxRangeValue.text]){
 
            action = [Wallet labelForBuyAction];
        }
        
    }// Segment Sell selected
    else if(self.segmentedControl.selectedSegmentIndex == 1){
    
         action = [Wallet labelForSellAction];
    }
    else{
        // Aucun bouton checké
        self.errorsCount++;
        self.mMessage = @"Sorry you must check an action, Buy Or Sell";
        [self displayError:self.mMessage];
    }
    
    if(self.errorsCount == 0){
        
        NSNumber *titlesFormated = [NSNumber numberWithInteger:[self.titlesNumberValue.text integerValue]];
        NSNumber *rangeMinFormated = [NSNumber numberWithInteger:[self.minRangeValue.text integerValue]];
        NSNumber *rangeMaxFormated = [NSNumber numberWithInteger:[self.maxRangeValue.text integerValue]];
    
        [self.delegate buySellTiltesGesture:YES action:action number:titlesFormated rangeMin:rangeMinFormated rangeMax:rangeMaxFormated];
    }
}

-(void)displayView {

    if(self.existTransaction == NO){
    
        [self.segmentedControl setEnabled:YES forSegmentAtIndex:0];
        [self.segmentedControl setEnabled:NO forSegmentAtIndex:1];
    }
    
    if(self.mWallet != nil){
 
        self.cashValue.text = [NSString stringWithFormat:@"%@ %@",[[self.mWallet cash] stringValue],[Config getDevise]];
    
    }
    
    if(self.mCompanyCourse != nil){
    
        self.stockPriceValue.text = [NSString stringWithFormat:@"%f %@",[self.mCompanyCourse floatValue],[Config getDevise]];
    }
    
}

-(BOOL) dataCheckIntegrity:(NSString*)action number:(NSString*)number rangeMin:(NSString*)rangeMin rangeMax:(NSString*)rangeMax {

    BOOL response = YES;
    NSString *message = [[NSString alloc] init];
    int numberInt = 0;
    
    if(![number isEqualToString:@""]){
    
        numberInt = [number intValue];
        
        if(numberInt <= 0){
        
            self.errorsCount = self.errorsCount + 1;
            response = NO;
            message = @"Sorry the number value is wrong";
            [self displayError:message];
        }
    
    }
    else{
    
        self.errorsCount = self.errorsCount + 1;
        response = NO;
        message = @"Sorry the number value is wrong";
        [self displayError:message];
    }
    
    if(![rangeMin isEqualToString:@""]){
        
        // On remplace la virgule par un point
        NSString *rangeMinNewString = [rangeMin stringByReplacingOccurrencesOfString:@"," withString:@"."];
        float rangeMinFloat = [rangeMinNewString floatValue];
        
        if(rangeMinFloat <= 0){
        
            self.errorsCount = self.errorsCount + 1;
            response = NO;
            message = @"Sorry the min range value is wrong";
            [self displayError:message];
        }
        
    }
    
    if(![rangeMax isEqualToString:@""]){
        
        // On remplace la virgule par un point
        NSString *rangeMaxNewString = [rangeMax stringByReplacingOccurrencesOfString:@"," withString:@"."];
        float rangeMaxFloat = [rangeMaxNewString floatValue];
        
        if(rangeMaxFloat <= 0){
            
            self.errorsCount = self.errorsCount + 1;
            response = NO;
            message = @"Sorry the max range value is wrong";
            [self displayError:message];
        }
        
    }
    
    if([action isEqualToString:[Wallet labelForSellAction]]){
        
        // On regarde si la quantité à vendre n'exède pas celle détenue.
        
        if(numberInt > [self.mCompanyTransactionQte intValue]){
        
            self.errorsCount = self.errorsCount + 1;
            response = NO;
            message = @"Sorry you don't have titles enough";
            [self displayError:message];
        
        }
    }
    
    
    if([action isEqualToString:[Wallet labelForBuyAction]]){
        
        // Cash nécessaire pour pouvoir acheter le nombre d'action indiqué dans le formulaire.
        float cashRequest = (float)numberInt * [self.mCompanyCourse floatValue];
        self.mWallet = nil;
        self.mWallet = [WalletMaster getWallet];
        
        if(cashRequest > [[self.mWallet cash] floatValue]){
        
            self.errorsCount = self.errorsCount + 1;
            response = NO;
            message = @"Sorry you don't have cash enough";
            [self displayError:message];
        }
        
    }

    return response;
}

-(void) displayError:(NSString *)message {

    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ooups!" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

@end
