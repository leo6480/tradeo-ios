//
//  NSString+withDecimals.m
//  tradeoProject
//
//  Created by student5305 on 12/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import "NSString+withDecimals.h"

@implementation NSString (withDecimals)

+ (NSString*)stringWithTwooDecimalsForFloatValue:(float)floatValue {

    return [NSString stringWithFormat:@"%.02f", floatValue];
}

+ (NSString*)stringWithTwooDecimalsForDoubleValue:(double)doubleValue {

    return [NSString stringWithFormat:@"%.02f", doubleValue];
}

@end
