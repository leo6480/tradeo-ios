//
//  OrderTask.m
//  tradeo
//
//  Created by Costabile Léonard on 8/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import "OrderTask.h"
#import "DbManager.h"
#import "OrderMaster.h"
#import "CompanyMaster+Gestion.h"
#import "Config.h"



@interface OrderTask()

@property (nonatomic, retain) NSMutableArray* orderList;

@end

@implementation OrderTask

-(id)init {
    
    self = [super init];
    
    if(self != nil){
        
    }
    
    return self;
}

- (void) startTimer:(int)seconds {

    // Lancement une première fois (le timer n'appelle pas directement remindTask)
    [self refreshOrder];
    [self check];
    
    self.timer = [NSTimer scheduledTimerWithTimeInterval:seconds
                                             target: self
                                             selector: @selector(remindTask:)
                                             userInfo: nil
                                             repeats: YES];
  
}

- (void) remindTask:(id)sender {

    [self refreshOrder];
    [self check];
}

- (void) refreshOrder {

    self.orderList = [NSMutableArray arrayWithArray:[[Config getDbManager] fetchAll:@"OrderMaster"]];
 
    if([self.orderList count] == 0){
    
        if ([self.timer isValid]){
            
            [self.timer invalidate];
        }
    }
}

- (void) check {
 
    for(OrderMaster *order in self.orderList){
        
        NSPredicate *where = [[Config getDbManager] where:@"tag=%@" value:[order companyTag]];
        NSArray *data = [[Config getDbManager] fetch:@"CompanyMaster" where:where];
        
        if([data count] != 0){
            
            CompanyMaster *company = [data objectAtIndex:0];
            NSString *action = [NSString stringWithString:[order action]];
            float stockPrice = [company getStockPrice]; 
            float orderPrice = [[order price] floatValue];
            float minRange = [[order minRange] floatValue];
            float maxRange = [[order maxRange] floatValue];
            
            if([self validOrder:action stockPrice:stockPrice orderPrice:orderPrice rangeMin:minRange rangeMax:maxRange]){
               
                if([action isEqualToString:@"Buy"]){
                    
                    [self.orderDelegate createTransactionWithAction:action companyTag:[order companyTag] numberTitle:[order number] stockPrice:[NSNumber numberWithFloat:stockPrice]];
                
                }
                else if([action isEqualToString:@"Sell"]){

                    float gain = [[order number] integerValue] * stockPrice;
                    [self.validSellTitlesDelegate updateCashWallet:[NSNumber numberWithFloat:gain]];
                    [self.validSellTitlesDelegate updateTransaction:[order companyTag] numberTitle:[order number]];
                }
                
                [self.orderDelegate refreshWallet];
                
                // Suppression de l'odre
                [[Config getDbManager] deleteObject:order];
     
            }
        }
    
    }

}

- (BOOL) validOrder:(NSString*)action stockPrice:(float)stockPrice orderPrice:(float)orderPrice rangeMin:(float)rangeMin rangeMax:(float)rangeMax{

    BOOL response = NO;
    
    if([action isEqualToString:@"Buy"]){
        
        // Ordres au prix du marché
        if(rangeMin == -1 && rangeMax == -1){
            
            if(stockPrice < orderPrice){
                response = true;
            }
        }
        else{
            //Ordres limités
            
            if(stockPrice >= rangeMin && stockPrice <= rangeMax){
                
                if(stockPrice < orderPrice){
                    response = true;
                }
            }
        }
    }
    else if([action isEqualToString:@"Sell"]){
        
        // Ordres au prix du marché
        if(rangeMin == -1 && rangeMax == -1){
            
            if(stockPrice > orderPrice){
                response = true;
            }
        }
        else{
            //Ordres limités
            
            if(stockPrice >= rangeMin && stockPrice <= rangeMax){
                
                if(stockPrice > orderPrice){
                    response = true;
                }
            }
        }
    }
    
    return response;
}


@end
