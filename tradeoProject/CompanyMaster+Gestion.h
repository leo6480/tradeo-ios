//
//  CompanyMaster+Gestion.h
//  tradeoProject
//
//  Created by student5305 on 12/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import "CompanyMaster.h"

@interface CompanyMaster (Gestion)

- (NSMutableArray*) getListFromString:(NSString*)stockEvolution;
- (NSString*) getStringFromList:(NSMutableArray*)stockEvolution;
- (NSArray*) getStringArrayFromList:(NSMutableArray*)stockEvolution;
- (NSMutableArray*) getStockEvolutionList;
- (void) setStockEvolutionWithList:(NSMutableArray*)stockEvolution;
- (float) getStockPrice;

@end
