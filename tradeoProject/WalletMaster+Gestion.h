//
//  WalletMaster+Gestion.h
//  tradeoProject
//
//  Created by student5305 on 12/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import "WalletMaster.h"

@interface WalletMaster (Gestion)

+ (id) getWallet;
- (float) getTotal;

@end
