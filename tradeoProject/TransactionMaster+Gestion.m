//
//  TransactionMaster+Gestion.m
//  tradeoProject
//
//  Created by student5305 on 12/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import "TransactionMaster+Gestion.h"
#import "TransactionHistMaster.h"
#import "DbManager.h"
#import "Config.h"
#import "CompanyMaster+Gestion.h"
#import "NSString+withDecimals.h"

@implementation TransactionMaster (Gestion)

-(void) calculate:(TransactionHistMaster*)transactionHist {
    
    NSInteger newNumberIV = [[self number] integerValue] + [[transactionHist number] integerValue];
    float currentTotal = [[self number] integerValue] * [[self unitPrice] floatValue];
    float transHistTotal = [[transactionHist number] integerValue] * [[transactionHist unitPrice] floatValue];
    float total = currentTotal + transHistTotal;
    float newUnitPriceFV = (total) / (newNumberIV);
    
    NSNumber *newNumber = [NSNumber numberWithInteger:newNumberIV];
    NSNumber *newUnitPrice = [NSNumber numberWithInteger:newUnitPriceFV];
    
    [self setNumber:newNumber];
    [self setUnitPrice:newUnitPrice];
    
}

-(NSNumber*) getTotal {
    
    self.total = 0;
    
    NSPredicate *where = [[Config getDbManager] where:@"tag=%@" value:[self companyTag]];
    NSArray *data = [[Config getDbManager] fetch:@"CompanyMaster" where:where];
    
    if([data count] != 0){
        
        CompanyMaster *company = [data objectAtIndex:0];
        NSInteger total = [company getStockPrice] * [[self number] integerValue];
        self.total = [NSNumber numberWithInteger:total];
        
    }
    
    NSString* formattedNumber = [NSString stringWithTwooDecimalsForFloatValue:self.total.floatValue];
    float floatTwoDecimalDigits = atof([formattedNumber UTF8String]);
    self.total = [NSNumber numberWithFloat:floatTwoDecimalDigits];
    
    return self.total;
    
}

-(void) calculateGain {
    
    float startValue = [[self number] integerValue] * [[self unitPrice] floatValue];
    NSString* formattedNumber = [NSString stringWithTwooDecimalsForFloatValue:startValue];
    startValue = atof([formattedNumber UTF8String]); // Pour forcer si nécessaire un nombre à 2 décimales
    float currentValue = [[self getTotal] floatValue];
    
    if(currentValue >= startValue){
        
        [self setPositive:[NSNumber numberWithBool:YES]];
    }
    else{
        [self setPositive:[NSNumber numberWithBool:NO]];
    }
    
    float moneyGain = currentValue - startValue;
    
    if(currentValue >= startValue){
        
        moneyGain = moneyGain - ((moneyGain / 100) * [[self taxePercent] floatValue]);
    }
    
    [self setMoneyGain:[NSNumber numberWithFloat:moneyGain]];
    
    float percentGain;
    
    if(currentValue >= startValue){
        
        percentGain = currentValue / startValue;
    }
    else{
        
        percentGain = startValue / currentValue;
    }
    
    [self setPercentGain:[NSNumber numberWithFloat:percentGain]];
    
}

@end
