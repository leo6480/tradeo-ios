//
//  HomeVC.m
//  tradeoProject
//
//  Created by Costabile Léonard on 9/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import "HomeVC.h"
#import "CompanyTableViewCell.h"
#import "CompanyTransactionVC.h"
#import "CompanyMaster.h"
#import "CompanyMaster+Gestion.h"
#import "NSString+withDecimals.h"
#import "Config.h"
#import "WalletMaster.h"
#import "OrderMaster.h"
#import "WalletMaster+Gestion.h"
#import "JBLineChartView.h"
#import "JBChartHeaderView.h"
#import "JBChartInformationView.h"
#import "JBConstants.h"
#import "StockEvolution.h"

// Numerics
CGFloat const kJBAreaChartViewControllerChartHeight = 88.0f;
CGFloat const kJBAreaChartViewControllerChartPadding = 10.0f;
CGFloat const kJBAreaChartViewControllerChartHeaderHeight = 75.0f;
CGFloat const kJBAreaChartViewControllerChartHeaderPadding = 20.0f;
CGFloat const kJBAreaChartViewControllerChartFooterHeight = 20.0f;
CGFloat const kJBAreaChartViewControllerChartLineWidth = 2.0f;
NSInteger const kJBAreaChartViewControllerMaxNumChartPoints = 12;
CGFloat const LineChartViewLabelViewWidth = 70.0f;
CGFloat const LineChartViewLabelViewHeight = 25.0f;
CGFloat const ScrollViewWidth = 320.0f;

// Strings
NSString * const kJBAreaChartViewControllerNavButtonViewKey = @"view";

@interface HomeVC () <JBLineChartViewDelegate, JBLineChartViewDataSource>

-(void)initDatabase;
-(void)initWallet;
-(void)initCompany;
-(void)fecthDataForStockEvolutionList;


@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) NSMutableArray *stockEvolutionList;
@property (nonatomic, strong) JBLineChartView *lineChartView;
@property (nonatomic) CGFloat scrollHeight;

@end

@implementation HomeVC

- (id)init
{
    self = [super init];
    if (self)
    {
        self.companiesList = [[NSMutableArray alloc] init];
        [self fecthDataForStockEvolutionList];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.companiesList = [[NSMutableArray alloc] init];
        [self fecthDataForStockEvolutionList];
    }
    return self;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"Home";
        self.companiesList = [[NSMutableArray alloc] init];
        [self fecthDataForStockEvolutionList];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.


    [self initDatabase];
    
    [self refreshView];
    
    [self fecthDataForStockEvolutionList];
    
    [self initLineChartView];
    
    [self initScrollView];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)initCompany {
    

    NSArray *data = [[Config getDbManager] fetchAll:@"CompanyMaster"];
    
    
    if([data count] == 0){
    
        CompanyMaster *twitter = (CompanyMaster*)[[Config getDbManager] initClass:@"CompanyMaster"];
        [twitter setName:@"Twitter"];
        [twitter setTag:@"TWRT"];
        [twitter setStockEvolution:@"40.45,41.02,41.34,41.80,41.50,40.45"];
        
        CompanyMaster *facebook = (CompanyMaster*)[[Config getDbManager] initClass:@"CompanyMaster"];
        [facebook setName:@"Facebook"];
        [facebook setTag:@"FB"];
        [facebook setStockEvolution:@"43.48,41.02,41.34,41.80,41.50,41.40"];
        
        CompanyMaster *linkedin = (CompanyMaster*)[[Config getDbManager] initClass:@"CompanyMaster"];
        [linkedin setName:@"Linkedin"];
        [linkedin setTag:@"LIK"];
        [linkedin setStockEvolution:@"42.45,41.02,41.34,41.80,41.50,43.05"];
        
        CompanyMaster *nescafe = (CompanyMaster*)[[Config getDbManager] initClass:@"CompanyMaster"];
        [nescafe setName:@"Nescafe"];
        [nescafe setTag:@"NS"];
        [nescafe setStockEvolution:@"40.45,41.02,41.34,41.80,41.50,40.45"];
        
        CompanyMaster *cp1 = (CompanyMaster*)[[Config getDbManager] initClass:@"CompanyMaster"];
        [cp1 setName:@"CP1"];
        [cp1 setTag:@"CP1"];
        [cp1 setStockEvolution:@"43.48,41.02,41.34,41.80,41.50,41.40"];
        
        CompanyMaster *cp2 = (CompanyMaster*)[[Config getDbManager] initClass:@"CompanyMaster"];
        [cp2 setName:@"CP2"];
        [cp2 setTag:@"CP2"];
        [cp2 setStockEvolution:@"42.45,41.02,41.34,41.80,41.50,43.05"];
        
        CompanyMaster *cp3 = (CompanyMaster*)[[Config getDbManager] initClass:@"CompanyMaster"];
        [cp3 setName:@"CP3"];
        [cp3 setTag:@"CP3"];
        [cp3 setStockEvolution:@"40.45,41.02,41.34,41.80,41.50,40.45"];
        
        CompanyMaster *cp4 = (CompanyMaster*)[[Config getDbManager] initClass:@"CompanyMaster"];
        [cp4 setName:@"CP4"];
        [cp4 setTag:@"CP4"];
        [cp4 setStockEvolution:@"43.48,41.02,41.34,41.80,41.50,41.40"];
        
        CompanyMaster *cp5 = (CompanyMaster*)[[Config getDbManager] initClass:@"CompanyMaster"];
        [cp5 setName:@"CP5"];
        [cp5 setTag:@"CP5"];
        [cp5 setStockEvolution:@"42.45,43.02,44.34,45.80,41.50,48.05"];
        
        [[Config getDbManager] saveObject];
    
    }
    
}

-(void)initWallet {
    
    // Création du portefeuille suite à l'installation et le premier lancement de l'application

    WalletMaster *walletMaster = [WalletMaster getWallet];
    
    if(walletMaster == nil){
    
        WalletMaster *walletMaster = (WalletMaster*)[[Config getDbManager] initClass:@"WalletMaster"];
        [walletMaster setCash:[Config getMinCashWallet]];
        [[Config getDbManager] saveObject];
    
    }
}

-(void)initDatabase {

    [self initCompany];
    [self initWallet];
}

-(void)refreshView {
    
    NSArray *data = [[Config getDbManager] fetchAll:@"CompanyMaster"];

    if([data count] != 0){
        
        [self.companiesList addObjectsFromArray:data];
    }
}

-(void)fecthDataForStockEvolutionList {
    
    self.stockEvolutionList = [[NSMutableArray alloc] init];
    
    for(int i = 0 ; i < self.companiesList.count ; i++){
        
        CompanyMaster *company = [self.companiesList objectAtIndex:i];
        StockEvolution *stock = [[StockEvolution alloc] init];
        NSMutableArray *mutableLineCharts = [NSMutableArray array];
        NSMutableArray *mutableChartData  = [company getStockEvolutionList];  // retourne NSMutableArray de float
        [mutableLineCharts addObject:mutableChartData];
        
        [stock setCompany:company];
        [stock setCharData:mutableLineCharts];
        stock.priceInfo = [company getStringArrayFromList:[company getStockEvolutionList]];
        
        [self.stockEvolutionList addObject:stock];
    }
}

-(void) initLineChartView {
    
    CGFloat mutableHeight = 0;
    self.scrollHeight = 0; // pour la scrollView
    
    for(int i = 0 ; i < self.stockEvolutionList.count ; i++){
        
        JBLineChartView *lineChartView = [[JBLineChartView alloc] init];
        lineChartView.frame = CGRectMake(kJBAreaChartViewControllerChartPadding,
                                         kJBAreaChartViewControllerChartPadding + mutableHeight,
                                         self.view.bounds.size.width - (kJBAreaChartViewControllerChartPadding * 2),
                                         kJBAreaChartViewControllerChartHeight);
        lineChartView.delegate = self;
        lineChartView.dataSource = self;
        lineChartView.headerPadding =kJBAreaChartViewControllerChartHeaderPadding;
        lineChartView.backgroundColor = kJBColorLineChartBackground;
        lineChartView.tag = i;
        
        mutableHeight = mutableHeight + kJBAreaChartViewControllerChartHeight + kJBAreaChartViewControllerChartPadding;
        
        if(self.lineChartView == nil){
            self.lineChartView = lineChartView;
        }
        
        // Création du label pour le tag de la company
        UILabel *tagLabel = [[UILabel alloc] initWithFrame:CGRectMake(lineChartView.bounds.origin.x, lineChartView.bounds.origin.y, LineChartViewLabelViewWidth, LineChartViewLabelViewHeight)];
        tagLabel.text = [[[self.stockEvolutionList objectAtIndex:i] company] tag];
        tagLabel.textColor = [UIColor whiteColor];
        tagLabel.backgroundColor = [UIColor blueColor];
        tagLabel.textAlignment = NSTextAlignmentCenter;
        [lineChartView addSubview:tagLabel];
        
        // Création du label pour le stock price de la company
        UILabel *stockPriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(lineChartView.bounds.size.width - LineChartViewLabelViewWidth, lineChartView.bounds.origin.y, LineChartViewLabelViewWidth, LineChartViewLabelViewHeight)];
        
        NSString *price = [NSString stringWithTwooDecimalsForFloatValue:[[[self.stockEvolutionList objectAtIndex:i] company] getStockPrice]];
        stockPriceLabel.text = [NSString stringWithFormat:@"%@ %@",price, [Config getDevise]];
        stockPriceLabel.textColor = [UIColor whiteColor];
        stockPriceLabel.backgroundColor = [UIColor blueColor];
        stockPriceLabel.textAlignment = NSTextAlignmentCenter;
        [lineChartView addSubview:stockPriceLabel];
        
        [self.scrollView addSubview:lineChartView];
        [lineChartView reloadData];
        
        self.scrollHeight += lineChartView.frame.size.height + kJBAreaChartViewControllerChartPadding;
        
    }
}

-(void) initScrollView {
    
    self.scrollView.delegate = self;
    self.scrollView.scrollEnabled = YES;
    self.scrollView.contentSize = CGSizeMake(ScrollViewWidth, self.scrollHeight);
    
}

#pragma mark - JBLineChartViewDataSource

- (NSUInteger)numberOfLinesInLineChartView:(JBLineChartView *)lineChartView
{
    NSInteger myInt = [lineChartView tag];
    NSUInteger unsignedInt = (NSUInteger)myInt;
    NSArray *data = [[self.stockEvolutionList objectAtIndex:unsignedInt] charData];
    return [data count];
}

- (NSUInteger)lineChartView:(JBLineChartView *)lineChartView numberOfVerticalValuesAtLineIndex:(NSUInteger)lineIndex
{
    
    NSInteger myInt = [lineChartView tag];
    NSUInteger unsignedInt = (NSUInteger)myInt;
    NSArray *data2 = [[self.stockEvolutionList objectAtIndex:unsignedInt] charData];
    return [[data2 objectAtIndex:lineIndex] count];
}

- (BOOL)lineChartView:(JBLineChartView *)lineChartView showsDotsForLineAtLineIndex:(NSUInteger)lineIndex
{
    return NO;
}

- (BOOL)lineChartView:(JBLineChartView *)lineChartView smoothLineAtLineIndex:(NSUInteger)lineIndex
{
    return YES;
}

#pragma mark - JBLineChartViewDelegate

- (CGFloat)lineChartView:(JBLineChartView *)lineChartView verticalValueForHorizontalIndex:(NSUInteger)horizontalIndex atLineIndex:(NSUInteger)lineIndex
{
    
    NSInteger myInt = [lineChartView tag];
    NSUInteger unsignedInt = (NSUInteger)myInt;
    NSArray *data3 = [[self.stockEvolutionList objectAtIndex:unsignedInt] charData];
    return [[[data3 objectAtIndex:lineIndex] objectAtIndex:horizontalIndex] floatValue];
}

- (void)lineChartView:(JBLineChartView *)lineChartView didSelectLineAtIndex:(NSUInteger)lineIndex horizontalIndex:(NSUInteger)horizontalIndex touchPoint:(CGPoint)touchPoint
{
    NSInteger myInt = [lineChartView tag];
    NSUInteger unsignedInt = (NSUInteger)myInt;
    [self setTooltipVisible:YES animated:YES atTouchPoint:touchPoint];
    
    NSArray *data4 = [[self.stockEvolutionList objectAtIndex:unsignedInt] priceInfo];
    [self.tooltipView setText:[data4 objectAtIndex:horizontalIndex]];
    
}

- (void)didDeselectLineInLineChartView:(JBLineChartView *)lineChartView
{
    NSInteger myInt = [lineChartView tag];
    StockEvolution *stockEvolution = [self.stockEvolutionList objectAtIndex:myInt];
    CompanyMaster *company = [stockEvolution company];
    CompanyTransactionVC *companyTransactionVC = [[CompanyTransactionVC alloc] initWithNibName:@"CompanyTransactionVC" bundle:nil];
    companyTransactionVC.mCompany = company;
    
    [self.navigationController pushViewController:companyTransactionVC animated:YES];
}

- (UIColor *)lineChartView:(JBLineChartView *)lineChartView colorForLineAtLineIndex:(NSUInteger)lineIndex
{
    return [UIColor greenColor];
}

- (UIColor *)lineChartView:(JBLineChartView *)lineChartView fillColorForLineAtLineIndex:(NSUInteger)lineIndex
{
    return [UIColor yellowColor];
}

- (UIColor *)lineChartView:(JBLineChartView *)lineChartView colorForDotAtHorizontalIndex:(NSUInteger)horizontalIndex atLineIndex:(NSUInteger)lineIndex
{
    return [UIColor redColor];
}

- (CGFloat)lineChartView:(JBLineChartView *)lineChartView widthForLineAtLineIndex:(NSUInteger)lineIndex
{
    return kJBAreaChartViewControllerChartLineWidth;
}

- (UIColor *)lineChartView:(JBLineChartView *)lineChartView verticalSelectionColorForLineAtLineIndex:(NSUInteger)lineIndex
{
    return [UIColor blackColor];
}

- (UIColor *)lineChartView:(JBLineChartView *)lineChartView selectionColorForLineAtLineIndex:(NSUInteger)lineIndex
{
    return [UIColor grayColor];
}

- (UIColor *)lineChartView:(JBLineChartView *)lineChartView selectionFillColorForLineAtLineIndex:(NSUInteger)lineIndex
{
    return [UIColor grayColor];
}


- (JBLineChartViewLineStyle)lineChartView:(JBLineChartView *)lineChartView lineStyleForLineAtLineIndex:(NSUInteger)lineIndex
{
    return JBLineChartViewLineStyleSolid;
}

#pragma mark - Overrides

- (JBChartView *)chartView
{
    return self.lineChartView;
}

@end
