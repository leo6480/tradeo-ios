//
//  OrderVC.m
//  tradeoProject
//
//  Created by Costabile Léonard on 10/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import "OrderVC.h"
#import "OrderTableViewCell.h"
#import "OrderMaster.h"
#import "Config.h"
#import "CompanyMaster+Gestion.h"
#import "NSString+withDecimals.h"

@interface OrderVC ()

@end

@implementation OrderVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"Order";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.ordersList = [[Config getDbManager] fetchAll:@"OrderMaster"];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"OrderTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"OrderCell"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.ordersList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    OrderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"OrderCell" forIndexPath:indexPath];
    
    OrderMaster *order = [self.ordersList objectAtIndex:indexPath.row];
    
    //Récupération de la company
    NSPredicate *where = [[Config getDbManager] where:@"tag=%@" value:[order companyTag]];
    NSArray *data = [[Config getDbManager] fetch:@"CompanyMaster" where:where];
    if([data count] > 0){
    
        CompanyMaster *company = [data objectAtIndex:0];
        cell.companyTag.text = order.companyTag;
        cell.actionSense.text = order.action;
        cell.titlesNumberValue.text = [order.number stringValue];
        NSString *stockPrice = [NSString stringWithTwooDecimalsForFloatValue:[company getStockPrice]];
        cell.stockPriceValue.text = [NSString stringWithFormat:@"%@ %@",stockPrice,[Config getDevise]];
        
        if([[order minRange] floatValue] == -1 || [[order minRange] floatValue] == 0){
            
            [cell.minRangeValue setHidden:YES];
        }
        else{
        
            NSString *min = [NSString stringWithTwooDecimalsForFloatValue:[[order minRange] floatValue]];
            cell.minRangeValue.text = [NSString stringWithFormat:@"%@ %@",min,[Config getDevise]];
        }
        
        if([[order maxRange] floatValue] == -1 || [[order maxRange] floatValue] == 0){
            
            [cell.maxRangeValue setHidden:YES];
        }
        else{
            
            NSString *max = [NSString stringWithTwooDecimalsForFloatValue:[[order maxRange] floatValue]];
            cell.maxRangeValue.text = [NSString stringWithFormat:@"%@ %@",max,[Config getDevise]];
        }
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 66;
}


@end
