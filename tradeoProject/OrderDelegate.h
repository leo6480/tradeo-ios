//
//  OrderDelegate.h
//  tradeo
//
//  Created by Costabile Léonard on 8/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol OrderDelegate <NSObject>

- (void) createTransactionWithAction:(NSString*)action companyTag:(NSString*)tag numberTitle:(NSNumber*)number stockPrice:(NSNumber*)price;

- (void) refreshWallet;

@end
