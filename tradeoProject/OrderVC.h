//
//  OrderVC.h
//  tradeoProject
//
//  Created by Costabile Léonard on 10/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderVC : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *ordersList;


@end
