//
//  DbManager.h
//  iOS7-ex5
//
//  Created by student5305 on 14/05/14.
//  Copyright (c) 2014 masociete. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface DbManager : NSObject

@property (retain, nonatomic) NSString *dbName;
@property (retain, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (retain, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (retain, nonatomic) NSPersistentStoreCoordinator *persistenceCoordinator;
@property (retain, nonatomic) NSString *documentsDirectoryPath;
@property (retain, nonatomic) NSPredicate *predicate;

- (id) initWithDbName:(NSString *)dbName;
- (NSEntityDescription *) getEntityDescriptionFor:(NSString *)entity;
- (id) initClass:(NSString *)className;
- (void) saveObject;
- (void) deleteObject:(NSObject *)object;
- (NSArray *) fetchAll:(NSString *)entity;
- (NSArray *) fetch:(NSString *)entity where:(id)whereObj;
- (NSPredicate*) where:(NSString*)format value:(id)value;
- (NSPredicate*) addWhere:(NSArray*)whereArray;

@end
