//
//  Wallet.h
//  tradeoProject
//
//  Created by student5305 on 13/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Wallet : NSObject

+(NSString*)labelForBuyAction;
+(NSString*)labelForSellAction;

@end
