//
//  HomeVC.h
//  tradeoProject
//
//  Created by Costabile Léonard on 9/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JBBaseChartViewController.h"

// Views
#import "JBChartTooltipView.h"
#import "JBChartView.h"


@interface HomeVC : JBBaseChartViewController <UIScrollViewDelegate>

@property (strong, nonatomic) NSMutableArray *companiesList;


@end
