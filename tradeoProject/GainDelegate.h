//
//  GainDelegate.h
//  tradeo
//
//  Created by Costabile Léonard on 9/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GainDelegate <NSObject>

- (void) updateGain;

@end
