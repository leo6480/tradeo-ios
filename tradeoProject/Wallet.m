//
//  Wallet.m
//  tradeoProject
//
//  Created by student5305 on 13/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import "Wallet.h"

@implementation Wallet

static NSString* buyAction = @"Buy";
static NSString* sellAction = @"Sell";

+(NSString*)labelForBuyAction {
    
    return buyAction;
}

+(NSString*)labelForSellAction {

    return sellAction;
}

@end
