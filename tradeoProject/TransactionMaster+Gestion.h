//
//  TransactionMaster+Gestion.h
//  tradeoProject
//
//  Created by student5305 on 12/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import "TransactionMaster.h"
#import "TransactionHistMaster.h"

@interface TransactionMaster (Gestion)

-(void) calculate:(TransactionHistMaster*)transactionHist;
-(NSNumber*) getTotal;
-(void) calculateGain;

@end
