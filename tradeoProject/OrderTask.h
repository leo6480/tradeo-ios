//
//  OrderTask.h
//  tradeo
//
//  Created by Costabile Léonard on 8/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OrderDelegate.h"
#import "ValidSellTitlesDelegate.h"

@interface OrderTask : NSObject

@property (nonatomic, retain) NSTimer *timer;
@property (nonatomic, assign) id<OrderDelegate> orderDelegate;
@property (nonatomic, assign) id<ValidSellTitlesDelegate> validSellTitlesDelegate;

- (id) init;
- (void) startTimer:(int)seconds;

@end
