//
//  TitlesBuySellVC.h
//  tradeoProject
//
//  Created by Costabile Léonard on 10/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TransactionMaster.h"
#import "CompanyMaster.h"
#import "WalletMaster.h"
#import "BuySellTiltesDelegate.h"

@interface TitlesBuySellVC : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *cashValue;
@property (strong, nonatomic) IBOutlet UILabel *stockPriceValue;
@property (strong, nonatomic) IBOutlet UISegmentedControl *actionValue;
@property (strong, nonatomic) IBOutlet UITextField *titlesNumberValue;
@property (strong, nonatomic) IBOutlet UITextField *minRangeValue;
@property (strong, nonatomic) IBOutlet UITextField *maxRangeValue;
@property (strong, nonatomic) TransactionMaster *mCompanyTransaction;
@property (strong, nonatomic) CompanyMaster *mCompany;
@property (strong, nonatomic) WalletMaster *mWallet;
@property (strong, nonatomic) NSNumber *mCompanyCourse; //float
@property (strong, nonatomic) NSNumber *mCompanyTransactionQte; //int
@property (strong, nonatomic) NSString *mMessage;
@property (nonatomic) int errorsCount;
@property (nonatomic) BOOL existTransaction;
@property (assign, nonatomic) id<BuySellTiltesDelegate> delegate;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;


- (IBAction)didTapCancel:(id)sender;
- (IBAction)didTapValidate:(id)sender;

@end
