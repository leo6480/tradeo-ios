//
//  AccountVC.m
//  tradeoProject
//
//  Created by Costabile Léonard on 9/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import "AccountVC.h"
#import "TransactionTableViewCell.h"
#import "CompanyTransactionVC.h"
#import "OrderVC.h"
#import "Config.h"
#import "WalletMaster.h"
#import "WalletMaster+Gestion.h"
#import "TransactionMaster.h"
#import "TransactionMaster+Gestion.h"
#import "NSString+withDecimals.h"
#import "OrderTask.h"
#import "GainTask.h"
#import "TransactionHistMaster.h"
#import "TransactionHistMaster+Gestion.h"
#import "CompanyMaster.h"

@interface AccountVC ()

@property (nonatomic) BOOL orderClockWainting;
@property (nonatomic, strong) OrderTask *orderTask;
@property (nonatomic, strong) GainTask *gainTask;

-(void)checkOrderWainting;
-(void)loadWallet;
-(void)updateWallet;

@end

@implementation AccountVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"Account";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Lancemement du timer pour la validation des orders
    self.orderTask = [[OrderTask alloc] init];
    [self.orderTask setOrderDelegate:self];
    [self.orderTask setValidSellTitlesDelegate:self];
    [self.orderTask startTimer:20];
    
    // Lancemement du timer pour la mise à jour des gains
    self.gainTask = [[GainTask alloc] init];
    [self.gainTask setGainDelegate:self];
    [self.gainTask startTimer:20];
    
    
    self.transactionsList = [[NSArray alloc] init];

    [self checkOrderWainting];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"TransactionTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"TransactionCell"];
    
    [self loadWallet];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated {

    [self.orderTask.timer invalidate];
    [self.gainTask.timer invalidate];
    
    self.orderTask = nil;
    self.gainTask = nil;
}

- (IBAction)didTapOrderWaiting:(id)sender {
    
    if(self.orderClockWainting == YES){
    
        OrderVC *orderVC = [[OrderVC alloc] initWithNibName:@"OrderVC" bundle:nil];
        
        [self.navigationController pushViewController:orderVC animated:YES];

    }
    
}

-(void)updateWallet {

     // Mise à jour du wallet en db
    
    WalletMaster *wallet = [WalletMaster getWallet];
    
    if(wallet != nil){
    
        float cashFloat = [[wallet cash] floatValue] - [wallet getTotal]; // cash restant (wallet.getTotal() = total à cet instant t)
        
        wallet.cash = [NSNumber numberWithFloat:cashFloat];
        
        [[Config getDbManager] saveObject];
  
    }

    [self refreshWallet];

}


#pragma mark - TableView Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.transactionsList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TransactionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TransactionCell" forIndexPath:indexPath];
    
    TransactionMaster *transaction = [self.transactionsList objectAtIndex:indexPath.row];
    
    cell.companyTag.text = [transaction companyTag];
    cell.titlesNumber.text = [[transaction number] stringValue];
    cell.unitPrice.text = [[transaction unitPrice] stringValue];
    
    [transaction calculateGain];

    NSString *operateur = nil;
    
    if([transaction.positive boolValue] == YES){
        
        cell.percentGain.textColor = [UIColor greenColor];
        operateur = @"+";
    }
    else{
        
        cell.percentGain.textColor = [UIColor redColor];
        operateur = @"-";
    }
    
    NSString *pg = [NSString stringWithTwooDecimalsForFloatValue:[[transaction percentGain] floatValue]];
    cell.percentGain.text = [NSString stringWithFormat:@"%@ %@ %%", operateur, pg];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    return 66;
}

#pragma mark - TableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    CompanyTransactionVC *companyTransactionVC = [[CompanyTransactionVC alloc] initWithNibName:@"CompanyTransactionVC" bundle:nil];
    
    TransactionMaster *transaction = [self.transactionsList objectAtIndex:indexPath.row];
    
    NSPredicate *where = [[Config getDbManager] where:@"tag=%@" value:[transaction companyTag]];
    NSArray *data = [[Config getDbManager] fetch:@"CompanyMaster" where:where];
    
    if([data count] != 0){
        
        CompanyMaster *company = [data objectAtIndex:0];
        companyTransactionVC.mCompany = company;
        [self.navigationController pushViewController:companyTransactionVC animated:YES];
    }

}

-(void)checkOrderWainting {
    
    self.orderClockWainting = NO;
    
    NSArray *orderList = [[Config getDbManager] fetchAll:@"OrderMaster"];
    UIImage *orderClock = [[UIImage alloc] init];
    
    if([orderList count] > 0){
    
        orderClock = [UIImage imageNamed:@"clockon48"];
        [self.buttonClock setBackgroundImage:orderClock forState:UIControlStateNormal];
        
        self.orderClockWainting = YES;
    }
    else{
    
        orderClock = [UIImage imageNamed:@"clockoff48"];
        [self.buttonClock setBackgroundImage:orderClock forState:UIControlStateNormal];
        self.orderClockWainting = NO;
    }
}

-(void)loadWallet {

    WalletMaster *wallet = [WalletMaster getWallet];
    
    if(wallet != nil){
    
        self.transactionsList = [[Config getDbManager] fetchAll:@"TransactionMaster"];
        [self.tableView reloadData];
        
        NSString *cashFormated = [NSString stringWithTwooDecimalsForFloatValue:[[wallet cash] floatValue]];
        NSString *walletFormated = [NSString stringWithTwooDecimalsForFloatValue:[wallet getTotal]];
        self.cashValue.text = [NSString stringWithFormat:@"%@ %@",cashFormated,[Config getDevise]];
        self.walletValue.text = [NSString stringWithFormat:@"%@ %@",walletFormated,[Config getDevise]];
    }
}

#pragma mark - OrderDelegate

//Un ordre d'achat a été validé et une nouvelle transaction doit être créé

- (void) createTransactionWithAction:(NSString*)action companyTag:(NSString*)tag numberTitle:(NSNumber*)number stockPrice:(NSNumber*)price {

    // 1 rechercher si il y a déjà une transaction pour cette company
    
    NSPredicate *whereT = [[Config getDbManager] where:@"companyTag=%@" value:tag];
    NSArray *dataT = [[Config getDbManager] fetch:@"TransactionMaster" where:whereT];
    
    if([dataT count] != 0){
    
        TransactionMaster *transaction = [dataT objectAtIndex:0];
        
        // Ajout de transactionHist sur transaction et recalcul
        TransactionHistMaster *transactionHist = (TransactionHistMaster*)[[Config getDbManager] initClass:@"TransactionHistMaster"];
        
        transactionHist.number = number;
        transactionHist.unitPrice = price;
        transactionHist.action = action;
        
        [transaction addTransactionHistListObject:transactionHist];
        [transaction calculate:transactionHist];
        
        // Mise à jour de la transaction
        [[Config getDbManager] saveObject];
        
        [self updateWallet];
   
    }
    else{
    
        // Récupération de la company
        
        NSPredicate *whereC = [[Config getDbManager] where:@"tag=%@" value:tag];
        NSArray *dataC = [[Config getDbManager] fetch:@"CompanyMaster" where:whereC];
        
        if([dataC count] != 0){
        
            CompanyMaster *company = [dataC objectAtIndex:0];
            
            // On va d'abord remplir l'objet transaction avec un transactionHist pour pouvoir faire les calcul
            // puis on l'insère, on rècupère son id pour le passer à transactionHist que l'on insère à la fin
            
             TransactionMaster *transaction = (TransactionMaster*)[[Config getDbManager] initClass:@"TransactionMaster"];
            transaction.companyTag = [company tag];
            transaction.companyName = [company name];
            transaction.transactionHistList = [[NSSet alloc] init];
            
            TransactionHistMaster *transactionHist = (TransactionHistMaster*)[[Config getDbManager] initClass:@"TransactionHistMaster"];
            
            transactionHist.number = number;
            transactionHist.unitPrice = price;
            transactionHist.action = action;
            
            [transaction addTransactionHistListObject:transactionHist];
            [transaction calculate:transactionHist];
            
            // Insertion de la transaction
            [[Config getDbManager] saveObject];
            
            [self updateWallet];
        
        }
    
    }
    
    [self refreshWallet];

}

- (void) refreshWallet {

    [self loadWallet];

}

#pragma mark - ValidSellTitlesDelegate

//Un ordre de vente a été validé et le wallet doit être mis à jour

- (void) updateCashWallet:(NSNumber*)gain {

    WalletMaster *wallet = [WalletMaster getWallet];
    
    if(wallet != nil){
    
        float currentCash = [[wallet cash] floatValue];
        float newCash = currentCash + [gain floatValue];
        wallet.cash = [NSNumber numberWithFloat:newCash];
        
        [[Config getDbManager] saveObject];
    }
    
  
}
- (void) updateTransaction:(NSString*)companyTag numberTitle:(NSNumber*)number {
 
    NSPredicate *whereT = [[Config getDbManager] where:@"companyTag=%@" value:companyTag];
    NSArray *dataT = [[Config getDbManager] fetch:@"TransactionMaster" where:whereT];
    
    if([dataT count] > 0){
    
        TransactionMaster *transaction = [dataT objectAtIndex:0];
        int newNumber = [[transaction number] intValue] - [number intValue];
        transaction.number = [NSNumber numberWithInt:newNumber];
        [[Config getDbManager] saveObject];
    }
    
    [self refreshWallet];
}

#pragma mark - GainDelegate

- (void) updateGain {

    [self loadWallet];
}

@end
