//
//  GainTask.h
//  tradeo
//
//  Created by Costabile Léonard on 9/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GainDelegate.h"

@interface GainTask : NSObject

@property (nonatomic, retain) NSTimer *timer;
@property (nonatomic, assign) id<GainDelegate> gainDelegate;

- (id) init;
- (void) startTimer:(int)seconds;

@end
