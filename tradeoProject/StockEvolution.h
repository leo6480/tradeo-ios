//
//  StockEvolution.h
//  tradeo
//
//  Created by Costabile Léonard on 8/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CompanyMaster.h"


@interface StockEvolution : NSObject

@property (nonatomic, retain) CompanyMaster *company;
@property (nonatomic, strong) NSArray *charData;
@property (nonatomic, strong) NSArray *priceInfo;

- (id) initWithCompany:(CompanyMaster*)company;

@end
