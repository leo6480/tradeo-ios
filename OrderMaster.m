//
//  OrderMaster.m
//  tradeoProject
//
//  Created by student5305 on 12/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import "OrderMaster.h"
#import "CompanyMaster.h"


@implementation OrderMaster

@dynamic action;
@dynamic companyTag;
@dynamic maxRange;
@dynamic minRange;
@dynamic number;
@dynamic price;
@dynamic company;

@end
