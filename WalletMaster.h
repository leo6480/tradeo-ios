//
//  WalletMaster.h
//  tradeoProject
//
//  Created by student5305 on 12/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class TransactionMaster;

@interface WalletMaster : NSManagedObject

@property (nonatomic, retain) NSString * buyAction;
@property (nonatomic, retain) NSNumber * cash;
@property (nonatomic, retain) NSString * noAction;
@property (nonatomic, retain) NSString * sellAction;
@property (nonatomic, retain) NSNumber * walletStartCash;
@property (nonatomic, retain) NSSet *transactionList;
@end

@interface WalletMaster (CoreDataGeneratedAccessors)

- (void)addTransactionListObject:(TransactionMaster *)value;
- (void)removeTransactionListObject:(TransactionMaster *)value;
- (void)addTransactionList:(NSSet *)values;
- (void)removeTransactionList:(NSSet *)values;

@end
