//
//  OrderMaster.h
//  tradeoProject
//
//  Created by student5305 on 12/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CompanyMaster;

@interface OrderMaster : NSManagedObject

@property (nonatomic, retain) NSString * action;
@property (nonatomic, retain) NSString * companyTag;
@property (nonatomic, retain) NSNumber * maxRange;
@property (nonatomic, retain) NSNumber * minRange;
@property (nonatomic, retain) NSNumber * number;
@property (nonatomic, retain) NSNumber * price;
@property (nonatomic, retain) CompanyMaster *company;

@end
