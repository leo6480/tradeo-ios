//
//  CompanyMaster.h
//  tradeoProject
//
//  Created by student5305 on 12/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface CompanyMaster : NSManagedObject

@property (nonatomic, retain) NSString * country;
@property (nonatomic, retain) NSNumber * importance;
@property (nonatomic, retain) NSNumber * lastUpdate;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * persistance;
@property (nonatomic, retain) NSNumber * secteur;
@property (nonatomic, retain) NSString * stockEvolution;
@property (nonatomic, retain) NSString * story;
@property (nonatomic, retain) NSString * tag;
@property (nonatomic, retain) NSNumber * typeBoursier;

@end
