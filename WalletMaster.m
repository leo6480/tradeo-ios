//
//  WalletMaster.m
//  tradeoProject
//
//  Created by student5305 on 12/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import "WalletMaster.h"
#import "TransactionMaster.h"


@implementation WalletMaster

@dynamic buyAction;
@dynamic cash;
@dynamic noAction;
@dynamic sellAction;
@dynamic walletStartCash;
@dynamic transactionList;

@end
