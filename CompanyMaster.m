//
//  CompanyMaster.m
//  tradeoProject
//
//  Created by student5305 on 12/08/14.
//  Copyright (c) 2014 technifutur.com. All rights reserved.
//

#import "CompanyMaster.h"


@implementation CompanyMaster

@dynamic country;
@dynamic importance;
@dynamic lastUpdate;
@dynamic name;
@dynamic persistance;
@dynamic secteur;
@dynamic stockEvolution;
@dynamic story;
@dynamic tag;
@dynamic typeBoursier;

@end
